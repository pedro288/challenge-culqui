# challenge-culqui

#Junior Pedro Pecho Mendoza

Path of  Swagger : http://localhost:8080/swagger-ui.html

##Request

```bash
GET /api/v1/tokens

{  
   "pan":"4444333322221111",
   "exp_year":2020,
   "exp_month":10
}
```

##Response

```bash
{  
   "token":"tkn_live_4444333322221111-2020-10",
   "brand":"visa",
   "creation_dt":"2019-01-01 18:00:00"
}
```
