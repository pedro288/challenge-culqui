package com.junior.pecho.culquitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TokenProcessApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokenProcessApplication.class, args);
	}

}
