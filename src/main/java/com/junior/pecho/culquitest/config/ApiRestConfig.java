package com.junior.pecho.culquitest.config;

import com.junior.pecho.culquitest.tokenprocess.api.impl.BinApi;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class ApiRestConfig {

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    @Bean
    BinApi binApi(@Value("${base.url.binlist}") String baseUrl) {
        return createService(BinApi.class, baseUrl);
    }

    public static <S> S createService(Class<S> serviceClass, String baseUrl) {

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create());
        httpClient.addInterceptor(logging);
        builder.client(httpClient.build());
        return builder.build().create(serviceClass);
    }
}
