package com.junior.pecho.culquitest.expose.web;


import com.junior.pecho.culquitest.tokenprocess.business.model.TokenRequest;
import com.junior.pecho.culquitest.tokenprocess.business.model.TokenResponse;
import com.junior.pecho.culquitest.tokenprocess.business.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @RequestMapping(value = "/tokens",method = RequestMethod.POST)
    public TokenResponse generateToken(@RequestBody TokenRequest request) throws IOException {
        log.info("Init TokenController.generateToken");
        return tokenService.generateToken(request);
    }
}
