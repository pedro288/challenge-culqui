package com.junior.pecho.culquitest.tokenprocess.api;

import com.junior.pecho.culquitest.tokenprocess.api.model.CardResponse;

import java.io.IOException;

public interface CustomerBinApi {


    CardResponse getInfoCard(String bin) throws IOException;
}
