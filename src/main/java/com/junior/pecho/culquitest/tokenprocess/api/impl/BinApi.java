package com.junior.pecho.culquitest.tokenprocess.api.impl;

import com.junior.pecho.culquitest.tokenprocess.api.model.CardResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BinApi {

    @GET("/{bin}")
    public Call<CardResponse> getCard(@Path("bin") String bin);
}
