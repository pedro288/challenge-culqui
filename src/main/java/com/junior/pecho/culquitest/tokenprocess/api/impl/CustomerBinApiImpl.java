package com.junior.pecho.culquitest.tokenprocess.api.impl;

import com.junior.pecho.culquitest.tokenprocess.api.CustomerBinApi;
import com.junior.pecho.culquitest.tokenprocess.api.model.CardResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class CustomerBinApiImpl implements CustomerBinApi {

    @Autowired
    private BinApi binApi;


    @Override
    public CardResponse getInfoCard(String bin) throws IOException {
        log.info("Init CustomerBinApiImpl.getInfoCard with bin {}",bin);
        return binApi.getCard(bin).execute().body();
    }
}
