package com.junior.pecho.culquitest.tokenprocess.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bank {
    private String name;
    private String url;
    private String phone;
    private String city;

}
