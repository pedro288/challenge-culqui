package com.junior.pecho.culquitest.tokenprocess.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardResponse {
    private CardNumber number;
    private String scheme;
    private String type;
    private String brand;
    private String preparid;
    private Country country;
    private Bank bank;
}
