package com.junior.pecho.culquitest.tokenprocess.business;

import com.junior.pecho.culquitest.tokenprocess.business.model.TokenRequest;
import com.junior.pecho.culquitest.tokenprocess.business.model.TokenResponse;

import java.io.IOException;

public interface TokenService {

    TokenResponse generateToken(TokenRequest tokenRequest) throws IOException;
}
