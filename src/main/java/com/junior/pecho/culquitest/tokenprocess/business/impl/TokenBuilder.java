package com.junior.pecho.culquitest.tokenprocess.business.impl;

import com.junior.pecho.culquitest.tokenprocess.business.model.TokenResponse;
import com.junior.pecho.culquitest.tokenprocess.api.model.CardResponse;
import com.junior.pecho.culquitest.tokenprocess.util.Util;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TokenBuilder {

    private static final String TOKEN_HEADER="tkn_live_";
    private static final String HYPHEN="-";

    public TokenResponse buildResponse(CardResponse cardResponse,String pan,Integer expirationYear,Integer expirationMonth) {

        return TokenResponse.builder()
                .token(TOKEN_HEADER.concat(pan).concat(HYPHEN)
                        .concat(expirationYear.toString())
                        .concat(HYPHEN)
                        .concat(expirationMonth.toString()))
                .brand(cardResponse.getScheme())
                .creation(Util.formatDate(LocalDateTime.now()))
                .build();

    }
}
