package com.junior.pecho.culquitest.tokenprocess.business.impl;

import com.junior.pecho.culquitest.tokenprocess.api.CustomerBinApi;
import com.junior.pecho.culquitest.tokenprocess.business.model.TokenRequest;
import com.junior.pecho.culquitest.tokenprocess.business.model.TokenResponse;
import com.junior.pecho.culquitest.tokenprocess.business.TokenService;
import com.junior.pecho.culquitest.tokenprocess.api.model.CardResponse;
import com.junior.pecho.culquitest.tokenprocess.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private CustomerBinApi customerBinApi;

    @Autowired
    private TokenBuilder tokenBuilder;


    @Override
    public TokenResponse generateToken(TokenRequest tokenRequest) throws IOException {
        log.info("Init TokenServiceImpl.generateToken");
        CardResponse cardResponse = customerBinApi.getInfoCard(Util.getBin(tokenRequest.getPan()));
        return tokenBuilder.buildResponse(cardResponse,tokenRequest.getPan(),
                tokenRequest.getExpirationYear(),
                tokenRequest.getExpirationMonth());
    }
}
