package com.junior.pecho.culquitest.tokenprocess.business.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class TokenRequest {

    @ApiModelProperty(
            example = "4444333322221111"
    )
    @JsonProperty("pan")
    @NotNull
    @NotEmpty
    @Size(min = 16,max = 16)
    private String pan;

    @ApiModelProperty(
            example = "2020"
    )
    @JsonProperty("exo_year")
    @NotNull
    @NotEmpty
    @Digits(integer = 4, fraction = 0)
    private Integer expirationYear;


    @ApiModelProperty(
            example = "10"
    )
    @JsonProperty("exp_mont")
    @NotNull
    @NotEmpty
    @Digits(integer = 2,fraction = 0)
    @Min(value = 1)
    @Max(value=12)
    private Integer expirationMonth;
}
