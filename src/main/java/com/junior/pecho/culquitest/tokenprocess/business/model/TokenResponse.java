package com.junior.pecho.culquitest.tokenprocess.business.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;


@Builder
@Getter
public class TokenResponse {

    @JsonProperty("tokenprocess")
    private String token;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("creation_dt")
    private String creation;
}
