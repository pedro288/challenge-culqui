package com.junior.pecho.culquitest.tokenprocess.util;

import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Util {

    private static final String FORMATTER_DATE= "yyyy-MM-dd HH:mm:ss";

    public static String getBin(String pan){
        if(!StringUtils.isEmpty(pan)){
            return pan.substring(0,6) ;
        }
        return null;
    }

    public static String formatDate(LocalDateTime date){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMATTER_DATE);

        return formatter.format(date);
    }
}
