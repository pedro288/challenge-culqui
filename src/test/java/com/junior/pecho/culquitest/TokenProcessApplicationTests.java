package com.junior.pecho.culquitest;

import com.junior.pecho.culquitest.expose.web.TokenController;
import com.junior.pecho.culquitest.tokenprocess.business.model.TokenRequest;
import com.junior.pecho.culquitest.tokenprocess.business.model.TokenResponse;
import com.junior.pecho.culquitest.tokenprocess.util.Util;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenProcessApplicationTests {


	@Autowired
	private TokenController tokenController;

	@Test
	public void contextLoads() throws IOException {
		TokenRequest tokenRequest= new TokenRequest();
		tokenRequest.setPan("4444333322221111");
		tokenRequest.setExpirationMonth(10);
		tokenRequest.setExpirationYear(2020);

		TokenResponse tokenResponse =tokenController.generateToken(tokenRequest);

		Assertions.assertThat(tokenResponse.getBrand()).isEqualTo("visa");
		Assertions.assertThat(tokenResponse.getToken()).isEqualTo("tkn_live_4444333322221111-2020-10");
		Assertions.assertThat(tokenResponse.getCreation()).isEqualTo(Util.formatDate(LocalDateTime.now()));
	}

}
